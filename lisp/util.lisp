(defmacro with-gensyms ((&rest names) &body body)
  `(let ,(let ((mapped-names nil)) (dolist (el names (nreverse mapped-names)) (push `(,el (gensym)) mapped-names)))
     ,@body))
