(in-package :common-lisp-user)
(defpackage :com.jpomidwest.bookcollector
  (:use :common-lisp)
  (:export :bkc-chapter
           :bkc-footnote
           :bkc-paragraph
           :bkc-subheading
           :bkc-verse))

(in-package :com.jpomidwest.bookcollector)

(defparameter bkc--element-index 0
  "A counter for the current element index to use when inserting")

(defparameter bkc--stream-out t
  "The location to which inserts should be written")

(defun bkc-set-index (&optional (index 0))
  "Reset the element index or set to a specific value."
  (setq bkc--element-index index))

(defun bkc-paragraph (text &optional
			     (index bkc--element-index index-supplied-p)
			     (out bkc--stream-out))
  "Creates an insert for a paragraph"
  ;; TODO
  (format out "Writing ~d characters at index ~d" (length text) index)
  (if (not index-supplied-p) (incf bkc--element-index))
  index)

(defun bkc-chapter (number title &optional
				   (index bkc--element-index index-supplied-p)
				   (out bkc--stream-out))
  "Creates an insert for a chapter"
  ;; TODO
  (format out "Writing chapter ~d (~a) at index ~d" number title index)
  (if (not index-supplied-p) (incf bkc--element-index))
  index)

(defun bkc-footnote (ref &optional text
			 (index bkc--element-index index-supplied-p)
			 (out bkc--stream-out))
  "Creates an insert for a footnote and/or sets the text for a footnote reference."
  ;; TODO
  (format out "Writing footnote ~a with text ~a at index ~d" ref text index)
  (if (not index-supplied-p) (incf bkc--element-index))
  index)

(defun bkc-subheading (text &optional
			      (index bkc--element-index index-supplied-p)
			      (out bkc--stream-out))
  "Creates an insert for a subheading"
  ;; TODO
  (format out "Writing subheading length ~d at index ~d" (length text) index)
  (if (not index-supplied-p) (incf bkc--element-index))
  index)

(defun bkc-verse (&key bk ch vs txt (val nil)
		    (index bkc--element-index index-supplied-p)
		    (out bkc--stream-out))
  "Creates an insert for a Scripture reference or quote"
  ;; TODO
  (format out "Writing verse ~a ~d:~d at index ~d" bk ch vs index)
  (if (not index-supplied-p) (incf bkc--element-index))
  index)


      
