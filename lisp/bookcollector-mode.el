;;; BookCollector minor mode for Emacs
;;;
;;; Usage:
;;; 1. Copy to a location from which you want to load files, e.g. ~/.emacs.d/elisp
;;; 2. Load from that directory in your ~/.emacs
;;;    Ex: (add-to-list 'load-path "~/.emacs.d/elisp")
;;; 3. For faster load time byte-compile the file
;;;    Ex: (byte-compile-file "~/.emacs.d/elisp/bookcollector-mode.el")
;;;    Emacs will automatically prefer the compiled .elc file if present.
;;;    The above command can be entered in the *scratch* buffer, or any
;;;    Lisp buffer, and run by placing the cursor at the end and entering
;;;    C-x C-e
;;; 4. Require the mode in your ~/.emacs : (require 'bookcollector-mode)
;;; 5. Enable in a buffer with M-x bookcollector-mode <return>

(defun bookcollector-insert-paragraph ()
  (interactive)
  (insert "(bkc-paragraph \""))

(defun bookcollector-insert-chapter ()
    (interactive)
  (insert "(bkc-chapter\n  :number \n  :title \""))

(defun bookcollector-insert-footnote ()
    (interactive)
  (insert "(bkc-footnote :ref "))

(defun bookcollector-insert-subheading ()
    (interactive)
    (insert "(bkc-subheading \""))

(defun bookcollector-insert-verse ()
  (interactive)
  (insert "(bkc-verse :bk :ch :vs :txt :val"))

(define-minor-mode bookcollector-mode
  "Minor mode for use with the bookcollector library."
  :keymap (let ((map (make-sparse-keymap)))
	    (define-key map (kbd "C-c p") 'bookcollector-insert-paragraph)
	    (define-key map (kbd "C-c c") 'bookcollector-insert-chapter)
	    (define-key map (kbd "C-c f") 'bookcollector-insert-footnote)
	    (define-key map (kbd "C-c s") 'bookcollector-insert-subheading)
	    (define-key map (kbd "C-c v") 'bookcollector-insert-verse)
	    map)
  :lighter " BkC")

(provide 'boockollector-mode)


