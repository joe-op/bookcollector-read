;;;; SQL testing / sandboxing

(load "~/.quicklisp/setup.lisp")
(ql:quickload :cl-dbi)

;;(:cl-dbi)

;; TODO: make this a macro using with-connection
;;   as in the README example
;;
;; (dbi:with-connection (conn :sqlite3 :database-name "/home/fukamachi/test.db")
;;   (let* ((query (dbi:prepare conn "SELECT * FROM People"))
;;          (query (dbi:execute query)))
;;     (loop for row = (dbi:fetch query)
;;           while row
;;           do (format t "~A~%" row))))

(defvar *connection*
  (dbi:connect :postgres
	       :database-name "books"
	       :username "bookcollector"
	       :password "ReadBooks88"))

(let* ((query (dbi:prepare *connection*
			   "SELECT b.title, t.value FROM \"Text\" t
JOIN \"Paragraph\" p on t.\"paragraphId\"=p.\"paragraphId\"
JOIN \"Book\" b on p.\"bookId\"=b.\"bookId\"
ORDER BY b.\"bookId\", p.\"seqNo\", t.\"seqNo\""))
       (query (dbi:execute query)))
  (loop for row = (dbi:fetch query)
     while row do
       (format t "~a~%" row)))
