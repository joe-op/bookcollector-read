# Notes

## Helpful Links

- [Quicklisp installation](https://www.quicklisp.org/beta/#installation)
- [PostgreSQL docs](https://www.postgresql.org/docs/)
- [cl-dbi homepage](https://github.com/fukamachi/cl-dbi)
